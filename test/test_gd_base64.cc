#include "gd/gd_base64.hh"
#include "gtest/gtest.h"

TEST ( Base64, Decoding_Encoding_1 ) {
  const auto decoded = "base64 encode decode !!!";
  const auto encoded = "YmFzZTY0IGVuY29kZSBkZWNvZGUgISEh";

  {
    auto [ out, size ] = gd::base64::encode ( decoded, ::strlen ( decoded ) );
    EXPECT_STREQ ( encoded, ( const char * ) out.get ( ) );
  }
  {
    auto [ out, size ] = gd::base64::decode ( encoded, ::strlen ( encoded ) );
    EXPECT_STREQ ( decoded, ( const char * ) out.get ( ) );
  }

  SUCCEED ( );
}

TEST ( Base64, Decoding_Encoding_2 ) {
  const auto decoded = "abc123!?$*&()'-=@~";
  const auto encoded = "YWJjMTIzIT8kKiYoKSctPUB+";

  {
    auto [ out, size ] = gd::base64::encode ( decoded, ::strlen ( decoded ) );
    EXPECT_STREQ ( encoded, ( const char * ) out.get ( ) );
  }

  {
    auto [ out, size ] = gd::base64::decode ( encoded, ::strlen ( encoded ) );
    EXPECT_STREQ ( decoded, ( const char * ) out.get ( ) );
  }

  SUCCEED ( );
}

TEST ( Base64, Decoding_Encoding_3 ) {
  const auto decoded = "TutorialsPoint?java8";
  const auto encoded = "VHV0b3JpYWxzUG9pbnQ/amF2YTg=";

  {
    auto [ out, size ] = gd::base64::encode ( decoded, ::strlen ( decoded ) );
    EXPECT_STREQ ( encoded, ( const char * ) out.get ( ) );
  }

  {
    auto [ out, size ] = gd::base64::decode ( encoded, ::strlen ( encoded ) );
    EXPECT_STREQ ( decoded, ( const char * ) out.get ( ) );
  }

  SUCCEED ( );
}


TEST ( Base64, Decoding_Encoding_4) {

{
  // NOTE: base64 tries to add a new line
    // -n in echo prevents it from adding a newline at the end of the input string.
    //-w 0 in base64 disables line wrapping, so the encoded output won’t contain a newline.
  // echo -n "The quick brown fox jumps over the lazy dog" | base64 -w 0
  const auto decoded = "The quick brown fox jumps over the lazy dog";
  const auto encoded = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZw==";

  {
    auto [ out, size ] = gd::base64::encode ( decoded, ::strlen ( decoded ) );
    EXPECT_STREQ ( encoded, ( const char * ) out.get ( ) );
  }

  {
    auto [ out, size ] = gd::base64::decode ( encoded, ::strlen ( encoded ) );
    EXPECT_STREQ ( decoded, ( const char * ) out.get ( ) );
  }
}
{
  // NOTE this will contain a new line when encoded
  // echo "The quick brown fox jumps over the lazy dog" | base64 
  const auto decoded = "The quick brown fox jumps over the lazy dog\n";
  const auto encoded = "VGhlIHF1aWNrIGJyb3duIGZveCBqdW1wcyBvdmVyIHRoZSBsYXp5IGRvZwo=";

  {
    auto [ out, size ] = gd::base64::encode ( decoded, ::strlen ( decoded ) );
    EXPECT_STREQ ( encoded, ( const char * ) out.get ( ) );
  }

  {
    auto [ out, size ] = gd::base64::decode ( encoded, ::strlen ( encoded ) );
    EXPECT_STREQ ( decoded, ( const char * ) out.get ( ) );
  }

}


  SUCCEED ( );
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
