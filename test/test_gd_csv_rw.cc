#include "gd/gd_csv.hh"
#include "gtest/gtest.h"

struct Data {
  std::string str;
  std::int8_t i8;
  std::int16_t i16;
  std::int32_t i32;
  std::int64_t i64;
};

class DataCsvStreamReader : public gd::csv::CsvStreamReader< Data > {
 public:
  DataCsvStreamReader ( std::istream &stream ) : CsvStreamReader< Data > ( stream ) {}

  Data toData ( gd::csv::CsvStringParser &&strParser ) override {
    Data data;
    strParser              //
        .add ( data.str )  //
        .add ( data.i8 )   //
        .add ( data.i16 )  //
        .add ( data.i32 )  //
        .add ( data.i64 );

    return data;
  }
};

class DataCsvStreamWriter : public gd::csv::CsvStreamWriter< Data > {
 public:
  DataCsvStreamWriter ( std::ostream &stream ) : CsvStreamWriter< Data > ( stream ) {}

  void fromData ( gd::csv::CsvStringBuilder &builder, const Data &data ) override {
    builder                 //
        .add ( data.str )   //
        .addDelimiter ( )   //
        .add ( data.i8 )    //
        .addDelimiter ( )   //
        .add ( data.i16 )   //
        .addDelimiter ( )   //
        .add ( data.i32 )   //
        .addDelimiter ( )   //
        .add ( data.i64 );  //
  }
};

auto func = [] ( auto hasHeader ) {
  Data dMin;
  dMin.str = "Min";
  dMin.i8 = INT8_MIN;
  dMin.i16 = INT16_MIN;
  dMin.i32 = INT32_MIN;
  dMin.i64 = INT64_MIN;

  Data dMax;
  dMax.str = "Max";
  dMax.i8 = INT8_MAX;
  dMax.i16 = INT16_MAX;
  dMax.i32 = INT32_MAX;
  dMax.i64 = INT64_MAX;

  std::ostringstream os;
  DataCsvStreamWriter csvStreamWriter ( os );

  if ( hasHeader ) {
    csvStreamWriter.writeHeader ( "Str", "i8", "i16", "i32", "i64" );
  }

  csvStreamWriter.write ( dMin );
  csvStreamWriter.write ( dMax );
  csvStreamWriter.write ( {dMin, dMax} );

  std::istringstream is ( os.str ( ) );

  DataCsvStreamReader csvStreamReader ( is );

  return csvStreamReader.read ( hasHeader );
};

TEST ( CSV, ReaderWriter ) {
  auto datalist = func ( false );

  EXPECT_EQ ( 4, datalist.size ( ) );

  EXPECT_EQ ( "Min", datalist[ 0 ].str );
  EXPECT_EQ ( INT8_MIN, datalist[ 0 ].i8 );
  EXPECT_EQ ( INT16_MIN, datalist[ 0 ].i16 );
  EXPECT_EQ ( INT32_MIN, datalist[ 0 ].i32 );
  EXPECT_EQ ( INT64_MIN, datalist[ 0 ].i64 );

  EXPECT_EQ ( "Max", datalist[ 1 ].str );
  EXPECT_EQ ( INT8_MAX, datalist[ 1 ].i8 );
  EXPECT_EQ ( INT16_MAX, datalist[ 1 ].i16 );
  EXPECT_EQ ( INT32_MAX, datalist[ 1 ].i32 );
  EXPECT_EQ ( INT64_MAX, datalist[ 1 ].i64 );

  EXPECT_EQ ( "Min", datalist[ 2 ].str );
  EXPECT_EQ ( INT8_MIN, datalist[ 2 ].i8 );
  EXPECT_EQ ( INT16_MIN, datalist[ 2 ].i16 );
  EXPECT_EQ ( INT32_MIN, datalist[ 2 ].i32 );
  EXPECT_EQ ( INT64_MIN, datalist[ 2 ].i64 );

  EXPECT_EQ ( "Max", datalist[ 3 ].str );
  EXPECT_EQ ( INT8_MAX, datalist[ 3 ].i8 );
  EXPECT_EQ ( INT16_MAX, datalist[ 3 ].i16 );
  EXPECT_EQ ( INT32_MAX, datalist[ 3 ].i32 );
  EXPECT_EQ ( INT64_MAX, datalist[ 3 ].i64 );

  SUCCEED ( );
}

TEST ( CSV, ReaderWriterHeader ) {
  auto datalist = func ( true );

  EXPECT_EQ ( 4, datalist.size ( ) );

  EXPECT_EQ ( "Min", datalist[ 0 ].str );
  EXPECT_EQ ( INT8_MIN, datalist[ 0 ].i8 );
  EXPECT_EQ ( INT16_MIN, datalist[ 0 ].i16 );
  EXPECT_EQ ( INT32_MIN, datalist[ 0 ].i32 );
  EXPECT_EQ ( INT64_MIN, datalist[ 0 ].i64 );

  EXPECT_EQ ( "Max", datalist[ 1 ].str );
  EXPECT_EQ ( INT8_MAX, datalist[ 1 ].i8 );
  EXPECT_EQ ( INT16_MAX, datalist[ 1 ].i16 );
  EXPECT_EQ ( INT32_MAX, datalist[ 1 ].i32 );
  EXPECT_EQ ( INT64_MAX, datalist[ 1 ].i64 );

  EXPECT_EQ ( "Min", datalist[ 2 ].str );
  EXPECT_EQ ( INT8_MIN, datalist[ 2 ].i8 );
  EXPECT_EQ ( INT16_MIN, datalist[ 2 ].i16 );
  EXPECT_EQ ( INT32_MIN, datalist[ 2 ].i32 );
  EXPECT_EQ ( INT64_MIN, datalist[ 2 ].i64 );

  EXPECT_EQ ( "Max", datalist[ 3 ].str );
  EXPECT_EQ ( INT8_MAX, datalist[ 3 ].i8 );
  EXPECT_EQ ( INT16_MAX, datalist[ 3 ].i16 );
  EXPECT_EQ ( INT32_MAX, datalist[ 3 ].i32 );
  EXPECT_EQ ( INT64_MAX, datalist[ 3 ].i64 );

  SUCCEED ( );
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
