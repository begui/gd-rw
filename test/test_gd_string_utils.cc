#include "gd/gd_string_utils.hh"
#include "gtest/gtest.h"

TEST ( StringUtils, RemoveAll ) {
  {
    std::string str = "This is a \"\"cool\"\" string";
    gd::string::replace_all ( str, "\"\"", "\"" );
    EXPECT_STREQ ( str.c_str ( ), "This is a \"cool\" string" );
  }
  SUCCEED ( );
}

TEST ( StringUtils, trimFront) {
  {
    std::string str = " \t\v\r\nThis is a \"cool\" string";

    std::string_view strView{str};
    str = gd::string::trim( str );
    EXPECT_STREQ ( str.c_str(), "This is a \"cool\" string" );
  }
  SUCCEED ( );
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
