#include <iostream>

#include "gd/gd_csv.hh"
#include "gtest/gtest.h"

TEST ( CSV, Read4String ) {
  auto expect_check = [] ( auto cells ) {
    EXPECT_EQ ( 4u, cells.size ( ) );
    EXPECT_EQ ( "this", cells[ 0 ] );
    EXPECT_EQ ( "is", cells[ 1 ] );
    EXPECT_EQ ( "a", cells[ 2 ] );
    EXPECT_EQ ( "test", cells[ 3 ] );
  };

  auto cells = gd::csv::parse ( "this,is,a,test", 4 );
  expect_check ( cells );
  cells = gd::csv::parse ( "this|is|a|test", 4, '|' );
  expect_check ( cells );
  cells = gd::csv::parse ( "this is a test", 4, ' ' );
  expect_check ( cells );

  SUCCEED ( );
}

TEST ( CSV, Read3StringWQuoteBegin ) {
  auto str = std::string ( R"("this,is",a,test)" );
  auto cells = gd::csv::parse ( str, 3 );
  EXPECT_EQ ( 3u, cells.size ( ) );
  EXPECT_EQ ( "this,is", cells[ 0 ] );
  EXPECT_EQ ( "a", cells[ 1 ] );
  EXPECT_EQ ( "test", cells[ 2 ] );
  SUCCEED ( );
}

TEST ( CSV, ReadStringWMissing1 ) {
  auto cells = gd::csv::parse ( ",is,a,test", 4 );
  EXPECT_EQ ( 4u, cells.size ( ) );
  EXPECT_EQ ( "", cells[ 0 ] );
  EXPECT_EQ ( "is", cells[ 1 ] );
  EXPECT_EQ ( "a", cells[ 2 ] );
  EXPECT_EQ ( "test", cells[ 3 ] );
  SUCCEED ( );
}

TEST ( CSV, ReadStringWMissing2 ) {
  auto cells = gd::csv::parse ( "this,,a,test", 4 );
  EXPECT_EQ ( 4u, cells.size ( ) );
  EXPECT_EQ ( "this", cells[ 0 ] );
  EXPECT_EQ ( "", cells[ 1 ] );
  EXPECT_EQ ( "a", cells[ 2 ] );
  EXPECT_EQ ( "test", cells[ 3 ] );
  SUCCEED ( );
}

TEST ( CSV, ReadStringWMissing3 ) {
  auto cells = gd::csv::parse ( "this,is,,test", 4 );
  EXPECT_EQ ( 4u, cells.size ( ) );
  EXPECT_EQ ( "this", cells[ 0 ] );
  EXPECT_EQ ( "is", cells[ 1 ] );
  EXPECT_EQ ( "", cells[ 2 ] );
  EXPECT_EQ ( "test", cells[ 3 ] );
  SUCCEED ( );
}
TEST ( CSV, ReadStringWMissing4 ) {
  auto cells = gd::csv::parse ( "this,is,a,", 4 );
  EXPECT_EQ ( 4u, cells.size ( ) );
  EXPECT_EQ ( "this", cells[ 0 ] );
  EXPECT_EQ ( "is", cells[ 1 ] );
  EXPECT_EQ ( "a", cells[ 2 ] );
  EXPECT_EQ ( "", cells[ 3 ] );
  SUCCEED ( );
}

TEST ( CSV, Read3StringWQuoteMiddle ) {
  auto str = std::string ( R"(this,"is,a",test)" );
  auto cells = gd::csv::parse ( str, 3 );
  EXPECT_EQ ( 3u, cells.size ( ) );
  EXPECT_EQ ( "this", cells[ 0 ] );
  EXPECT_EQ ( "is,a", cells[ 1 ] );
  EXPECT_EQ ( "test", cells[ 2 ] );
  SUCCEED ( );
}
TEST ( CSV, Read3StringWQuoteEnd ) {
  auto str = std::string ( R"(this,is,",a,test")" );
  auto cells = gd::csv::parse ( str, 3 );
  EXPECT_EQ ( 3u, cells.size ( ) );
  EXPECT_EQ ( "this", cells[ 0 ] );
  EXPECT_EQ ( "is", cells[ 1 ] );
  EXPECT_EQ ( ",a,test", cells[ 2 ] );
  SUCCEED ( );
}
TEST ( CSV, Read3StringWQuoteExpectException ) {
  try {
    auto str = std::string ( R"(this,is,",a,test)" );
    auto cells = gd::csv::parse ( str, 3 );
    FAIL ( ) << "Expecting Runtime Exception " << std::endl;
  } catch ( const std::runtime_error &e ) {
    // We are expecting this to happen
    SUCCEED ( ) << e.what ( );
  } catch ( ... ) {
    FAIL ( ) << "Expecting Runtime Exception" << std::endl;
  }
}

TEST ( CSV, CsvStringParser ) {
  gd::csv::CsvStringParser strParser ( R"("this,is",4,3.14, 3.15f)" );

  std::string cell1;
  std::int32_t cell2;
  double cell3;
  float cell4;

  strParser >> cell1 >> cell2 >> cell3 >> cell4;
  EXPECT_EQ ( "this,is", cell1 );
  EXPECT_EQ ( 4, cell2 );
  EXPECT_EQ ( 3.14, cell3 );
  EXPECT_EQ ( 3.15f, cell4 );

  SUCCEED ( );
}

TEST ( CSV, Csv_PARSER_BUILDER_LargeData_Item ) {
  constexpr auto str =
      "Sub-Saharan Africa,South "
      "Africa,Fruits,Offline,M,7/27/2012,443368995,7/28/2012,1593,9.33,6.92,14862.69,11023.56,3839.136358";

  gd::csv::CsvStringParser strParser ( str );

  std::string region;
  std::string county;
  std::string itemtype;
  std::string salesChannel;
  std::string orderPriority;
  std::string orderDate;
  std::int64_t orderId;
  std::string shipDate;
  std::int32_t unitsold;
  float unitPrice;
  float unitPriceTotal;
  double totalRev;
  double totalCost;
  double totalProfit;

  strParser >> region;
  strParser >> county;
  strParser >> itemtype;
  strParser >> salesChannel;
  strParser >> orderPriority;
  strParser >> orderDate;
  strParser >> orderId;
  strParser >> shipDate;
  strParser >> unitsold;
  strParser >> unitPrice;
  strParser >> unitPriceTotal;
  strParser >> totalRev;
  strParser >> totalCost;
  strParser >> totalProfit;

  EXPECT_EQ ( "Sub-Saharan Africa", region );
  EXPECT_EQ ( "South Africa", county );
  EXPECT_EQ ( "Fruits", itemtype );
  EXPECT_EQ ( "Offline", salesChannel );
  EXPECT_EQ ( "M", orderPriority );
  EXPECT_EQ ( "7/27/2012", orderDate );
  EXPECT_EQ ( 443368995, orderId );
  EXPECT_EQ ( "7/28/2012", shipDate );
  EXPECT_EQ ( 1593, unitsold );
  EXPECT_FLOAT_EQ ( 9.33, unitPrice );
  EXPECT_FLOAT_EQ ( 6.92, unitPriceTotal );
  EXPECT_DOUBLE_EQ ( 14862.69, totalRev );
  EXPECT_DOUBLE_EQ ( 11023.56, totalCost );
  EXPECT_DOUBLE_EQ ( 3839.136358, totalProfit );

  gd::csv::CsvStringBuilder strBuilder;
  strBuilder
      .add ( region )          //
      .addDelimiter ( )        //
      .add ( county )          //
      .addDelimiter ( )        //
      .add ( itemtype )        //
      .addDelimiter ( )        //
      .add ( salesChannel )    //
      .addDelimiter ( )        //
      .add ( orderPriority )   //
      .addDelimiter ( )        //
      .add ( orderDate )       //
      .addDelimiter ( )        //
      .add ( orderId )         //
      .addDelimiter ( )        //
      .add ( shipDate )        //
      .addDelimiter ( )        //
      .add ( unitsold )        //
      .addDelimiter ( )        //
      .add ( unitPrice )       //
      .addDelimiter ( )        //
      .add ( unitPriceTotal )  //
      .addDelimiter ( )        //
      .add ( totalRev )        //
      .addDelimiter ( )        //
      .add ( totalCost )       //
      .addDelimiter ( )        //
      .add ( totalProfit );     //

  EXPECT_EQ ( str, strBuilder.stream().str());

  SUCCEED ( );
}

TEST ( CSV, CsvStringParserAdd ) {
  gd::csv::CsvStringParser strParser ( R"("this,is",4,3.14, 3.15f)" );

  std::string cell1;
  std::int32_t cell2;
  double cell3;
  float cell4;

  strParser.add ( cell1 ).add ( cell2 ).add ( cell3 ).add ( cell4 );

  EXPECT_EQ ( "this,is", cell1 );
  EXPECT_EQ ( 4, cell2 );
  EXPECT_EQ ( 3.14, cell3 );
  EXPECT_EQ ( 3.15f, cell4 );

  SUCCEED ( );
}

TEST ( CSV, CsvStringParserMissingCell2 ) {
  gd::csv::CsvStringParser strParser ( R"("this,is a",,4,3.14, 3.15f)" );

  std::string cell1;
  std::string cell2;
  std::int32_t cell3;
  double cell4;
  float cell5;

  strParser >> cell1 >> cell2 >> cell3 >> cell4 >> cell5;
  EXPECT_EQ ( "this,is a", cell1 );
  EXPECT_EQ ( "", cell2 );
  EXPECT_EQ ( 4, cell3 );
  EXPECT_EQ ( 3.14, cell4 );
  EXPECT_EQ ( 3.15f, cell5 );

  SUCCEED ( );
}
TEST ( CSV, CsvStringParserMissingCell3 ) {
  gd::csv::CsvStringParser strParser ( R"("this,is a",test,,3.14, 3.15f)" );

  std::string cell1;
  std::string cell2;
  std::int32_t cell3;
  double cell4;
  float cell5;

  strParser >> cell1 >> cell2 >> cell3 >> cell4 >> cell5;
  EXPECT_EQ ( "this,is a", cell1 );
  EXPECT_EQ ( "test", cell2 );
  EXPECT_EQ ( 0, cell3 );
  EXPECT_EQ ( 3.14, cell4 );
  EXPECT_EQ ( 3.15f, cell5 );

  SUCCEED ( );
}

TEST ( CSV, CsvStringParserMissingCell4 ) {
  gd::csv::CsvStringParser strParser ( R"("this,is a",test,4,, 3.15f)" );

  std::string cell1;
  std::string cell2;
  std::int32_t cell3;
  double cell4{0.0};
  float cell5{0.0f};

  strParser >> cell1 >> cell2 >> cell3 >> cell4 >> cell5;
  EXPECT_EQ ( "this,is a", cell1 );
  EXPECT_EQ ( "test", cell2 );
  EXPECT_EQ ( 4, cell3 );
  EXPECT_EQ ( 0.0, cell4 );
  EXPECT_EQ ( 3.15f, cell5 );

  SUCCEED ( );
}

TEST ( CSV, CsvStringParserMissingCell5 ) {
  gd::csv::CsvStringParser strParser ( R"("aaa","b""bb","ccc")" );

  std::string cell1;
  std::string cell2;
  std::string cell3;

  strParser >> cell1 >> cell2 >> cell3;
  EXPECT_EQ ( "aaa", cell1 );
  EXPECT_EQ ( "b\"bb", cell2 );
  EXPECT_EQ ( "ccc", cell3 );

  SUCCEED ( );
}

// Fields with embedded commas or double-quote characters must be quoted.
TEST ( CSV, CsvStringParserMultipleQuote1 ) {
  gd::csv::CsvStringParser strParser ( R"(1997,Ford,E350,"Super, luxurious truck")" );

  std::int32_t cell1;
  std::string cell2;
  std::string cell3;
  std::string cell4;

  strParser >> cell1 >> cell2 >> cell3 >> cell4;
  EXPECT_EQ ( 1997, cell1 );
  EXPECT_EQ ( "Ford", cell2 );
  EXPECT_EQ ( "E350", cell3 );
  EXPECT_EQ ( "Super, luxurious truck", cell4 );

  SUCCEED ( );
}

// Fields with embedded commas or double-quote characters must be quoted.
TEST ( CSV, CsvStringParserMultipleQuote2 ) {
  gd::csv::CsvStringParser strParser ( R"(1997,Ford,E350,"Super, ""luxurious"" truck")" );

  std::int32_t cell1;
  std::string cell2;
  std::string cell3;
  std::string cell4;

  strParser >> cell1 >> cell2 >> cell3 >> cell4;
  EXPECT_EQ ( 1997, cell1 );
  EXPECT_EQ ( "Ford", cell2 );
  EXPECT_EQ ( "E350", cell3 );
  EXPECT_EQ ( "Super, \"luxurious\" truck", cell4 );

  SUCCEED ( );
}

TEST ( CSV, CsvStringParserMultipleQuote3 ) {
  gd::csv::CsvStringParser strParser ( R"(1997,Ford,E350,"Super, ""luxurious"" and ""awesome"" truck")" );

  std::int32_t cell1;
  std::string cell2;
  std::string cell3;
  std::string cell4;

  strParser >> cell1 >> cell2 >> cell3 >> cell4;
  EXPECT_EQ ( 1997, cell1 );
  EXPECT_EQ ( "Ford", cell2 );
  EXPECT_EQ ( "E350", cell3 );
  EXPECT_EQ ( "Super, \"luxurious\" and \"awesome\" truck", cell4 );

  SUCCEED ( );
}

TEST ( CSV, CsvStringBuilder ) {
  gd::csv::CsvStringBuilder builder;

  builder << 123;
  builder
      .addDelimiter ( )               //
      .add ( 123.4321f )              //
      .addDelimiter ( )               //
      .add ( "This \"is a Test\"" );  //

  EXPECT_EQ ( "123,123.432,This \"is a Test\"", builder.stream ( ).str ( ) );
}

int main ( int argc, char **argv ) {
  testing::InitGoogleTest ( &argc, argv );
  return RUN_ALL_TESTS ( );
}
