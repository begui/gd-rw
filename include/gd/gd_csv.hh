#pragma once

#include <charconv>
#include <iostream>
#include <sstream>
#include <string_view>
#include <vector>

#include "gd/gd_string_utils.hh"

//
// https://tools.ietf.org/html/rfc4180
//
namespace gd::csv {
std::vector< std::string_view > parse ( std::vector< std::string_view > &cells,  //
                                        const std::string &str,                  //
                                        const char delimiter = ',' );
std::vector< std::string_view > parse ( const std::string &str, const size_t elements, const char delimiter = ',' );

class CsvStringParser {
 public:
  explicit CsvStringParser ( std::string &&str ) : CsvStringParser ( std::move ( str ), ',' ) {}

  CsvStringParser ( std::string &&str, char delimiter ) : str_ ( std::move ( str ) ) {
    gd::csv::parse ( cells_, str_, delimiter );
    iter_ = cells_.begin ( );
  }

 private:
  const std::string str_;
  std::vector< std::string_view > cells_;
  std::vector< std::string_view >::const_iterator iter_;

 public:
  template < typename T >
  CsvStringParser &operator>> ( T &val ) {
    if constexpr ( std::is_integral_v< T > ) {
      val = 0;
      std::from_chars ( ( *iter_ ).data ( ), ( *iter_ ).data ( ) + ( *iter_ ).size ( ), val );
    }
    if constexpr ( std::is_floating_point_v< T > ) {
      // val = 0.0;
      // std::from_chars ( ( *iter_ ).data ( ), ( *iter_ ).data ( ) + ( *iter_ ).size ( ), val );

      // TODO: can't use string_view with std::sto* due to null termintor
      // so i need to create a fucking temp string cause gcc doesn't support the above yet
      if constexpr ( std::is_same_v< T, float > ) {
        val = !( *iter_ ).empty ( ) ? std::stof ( std::string ( ( *iter_ ).data ( ) ) ) : 0.f;
      }
      if constexpr ( std::is_same_v< T, double > ) {
        val = !( *iter_ ).empty ( ) ? std::stod ( std::string ( ( *iter_ ).data ( ) ) ) : 0.0;
      }
      if constexpr ( std::is_same_v< T, long double > ) {
        val = !( *iter_ ).empty ( ) ? std::stold ( std::string ( ( *iter_ ).data ( ) ) ) : 0.0;
      }
    }
    if constexpr ( std::is_same_v< T, std::string > ) {
      // const std::regex ESCAPEQUOTE ( "\"\"" );
      // Super super super slow
      // val = std::regex_replace((*iter_).data(), ESCAPEQUOTE, "\"");
      // Super slow
      // val = std::regex_replace ( val, ESCAPEQUOTE, "\"" );
      val = *iter_;
      gd::string::replace_all ( val, "\"\"", "\"" );
    }

    ++iter_;
    return *this;
  }

  template < typename T >
  CsvStringParser &add ( T &t ) {
    return *this >> t;
  }
};

class CsvStringBuilder final {
 public:
  explicit CsvStringBuilder ( char delimiter = ',' ) : delimiter_ ( delimiter ){};

 public:
  template < typename T >
  CsvStringBuilder &operator<< ( const T &t ) {
    oss_ << t;
    return *this;
  }

 public:
  template < typename T >
  CsvStringBuilder &add ( const T &t ) noexcept {
    if constexpr ( std::is_same_v< T, std::int8_t > ) {
      oss_ << static_cast< std::int16_t > ( t );
    } else if constexpr ( std::is_same_v< T, std::uint8_t > ) {
      oss_ << static_cast< std::uint16_t > ( t );
    } else if constexpr ( std::is_floating_point_v< T > ) {
      // TODO: Fix To keep precision, this is probably shit and will add trialing zeros
      oss_ << std::fixed << t;
    } else {
      oss_ << t;
    }
    return *this;
  }
  CsvStringBuilder &addDelimiter ( ) noexcept {
    oss_ << delimiter_;
    return *this;
  }
  CsvStringBuilder &addNewLine ( ) noexcept {
    oss_ << "\n";
    return *this;
  }

  std::ostringstream &stream ( ) { return oss_; }

 private:
  std::ostringstream oss_;
  char delimiter_;
};

template < typename T >
class CsvStreamReader {
 public:
  explicit CsvStreamReader ( std::istream &stream, char delimiter = ',' )
      : stream_ ( stream ), defaultDelimiter_ ( delimiter ) {}

 public:
  std::vector< T > read ( bool hasHeader = false ) {
    std::vector< T > results;
    std::string line;
    if ( hasHeader ) {
      // We just skip header for now until we build something smarter
      std::getline ( stream_, line );
    }
    while ( std::getline ( stream_, line ) ) {
      // Note: Becareful, a lot of allocations are done at this point and can get pretty large for large files
      results.emplace_back ( toData ( {std::move ( line ), defaultDelimiter_} ) );
    }
    return results;
  }
  // TODO: add read method with callback intead of returning massing list of data

 protected:
  virtual T toData ( CsvStringParser &&strParser ) = 0;

 private:
  std::istream &stream_;
  char defaultDelimiter_{','};
};

template < typename T >
class CsvStreamWriter {
 public:
  explicit CsvStreamWriter ( std::ostream &stream, char delimiter = ',' )
      : stream_ ( stream ), defaultDelimiter_ ( delimiter ) {}

  template < typename... TArgs >
  void writeHeader ( TArgs &&... args ) {
    ( ( stream_ << args << defaultDelimiter_ ), ... );
    stream_.seekp ( -1, stream_.cur );
    stream_ << '\n';
  }
  void write ( const std::vector< T > data ) {
    for ( std::size_t i = 0; i < data.size ( ) - 1; ++i ) {
      write ( data[ i ], true );
    }
    write ( data[ data.size ( ) - 1 ], false );
  }
  void write ( const T &data, const bool addNewLine = true ) {
    CsvStringBuilder builder ( defaultDelimiter_ );
    fromData ( builder, data );
    if ( addNewLine ) {
      builder.addNewLine ( );
    }
    stream_ << builder.stream ( ).str ( );
  }
  char getDelimiter ( ) const { return defaultDelimiter_; }
  virtual void fromData ( CsvStringBuilder &builder, const T &data ) = 0;

 private:
  std::ostream &stream_;
  char defaultDelimiter_{','};
};

}  // namespace gd::csv
