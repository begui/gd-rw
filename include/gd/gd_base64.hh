#pragma once
#include <cstddef>
#include <memory>
#include <tuple>
///
// Inspired from
//
// http://www.sunshine2k.de/articles/coding/base64/understanding_base64.html
//
namespace gd::base64 {

using Base64_t = std::tuple<std::unique_ptr<uint8_t[]>, std::size_t>;

constexpr bool is_base64_char(int ch) noexcept;

/**
 *  Base64_t encode ( const char *inData, const std::size_t inDataLength ) noexcept;
 *
 * @brief Encodes a string and returns its base64 representation
 *
 * @detail
 *
 *
 * @param inData An array of characters.
 * @param inDataLength The length of the array.
 *
 * @return Encoded value of the string
 *
 */
Base64_t encode(const char* inData, const std::size_t inDataLength) noexcept;
Base64_t encode(const uint8_t* inData, const std::size_t inDataLength) noexcept;
/**
 * Base64_t decode ( const char *inData, const std::size_t inDataLength ) noexcept;
 *
 * @brief Decodes value back to String
 *
 * @detail
 * Takes one character at the time and convert it to the bits that it represents.
 *
 * @example
 * A character would translate into 000000 and the / character translates into 111111.
 * Then concatenate the bits to get 000000 | 111111.
 * Because this won't fit into a byte, you have to split up and shift the
 * result to get 00000011 and 1111xxxx where xxxx is not known yet
 *
 * @param inData An array of characters.
 * @param inDataLength The length of the array.
 *
 * @return Decoded value of the string
 *
 */
Base64_t decode(const char* inData, const std::size_t inDataLength) noexcept;
Base64_t decode(const uint8_t* inData, const std::size_t inDataLength) noexcept;

} // namespace gd::base64
