#pragma once
#include <string>
#include <string_view>

namespace gd::string {

void replace_all ( std::string &str, const std::string &searchStr, const std::string &replaceStr ) noexcept;
std::string_view trim ( std::string_view strView ) noexcept;

}  // namespace gd::string
