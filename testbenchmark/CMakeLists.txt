##########################################################
#--------------------------------------------------------
# Tests Benchmark 
#--------------------------------------------------------
##########################################################
set( TESTBENCHMAKR_SRC
  testbenchmark_csv_rw.cc
  )

set( CMAKE_EXPORT_COMPILE_COMMANDS ON )

foreach(SRC_LINE ${TESTBENCHMAKR_SRC})
  string(REPLACE ".cc" "" FILE_LINE ${SRC_LINE})
  add_executable(${FILE_LINE} ${SRC_LINE})
  add_test(${FILE_LINE} ${FILE_LINE})

  target_link_libraries (${FILE_LINE}
    PRIVATE 
    GDRW
    benchmark::benchmark
    )
  target_compile_features(${FILE_LINE}
    PRIVATE
    cxx_std_17
    )
endforeach()
