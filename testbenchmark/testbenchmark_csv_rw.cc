#include <fstream>
#include <iostream>
#include <string>

#include "benchmark/benchmark.h"
#include "gd/gd_csv.hh"

//************************
// Note:
// Navigate to the url
// http://eforexcel.com/wp/downloads-18-sample-csv-files-data-sets-for-testing-sales/
// Download the data and drop
// Delete the header
//************************

struct Data {
  std::int32_t unitsold;
  std::int64_t orderId;
  float unitPrice;
  float unitPriceTotal;
  double totalRev;
  double totalCost;
  double totalProfit;
  std::string region;
  std::string county;
  std::string itemtype;
  std::string salesChannel;
  std::string orderPriority;
  std::string orderDate;
  std::string shipDate;
};

class DataCsvStreamReader final : public gd::csv::CsvStreamReader< Data > {
 public:
  DataCsvStreamReader ( std::istream &stream ) : CsvStreamReader< Data > ( stream ) {}

  Data toData ( gd::csv::CsvStringParser &&strParser ) override {
    Data data;
    strParser                         //
        .add ( data.region )          //
        .add ( data.county )          //
        .add ( data.itemtype )        //
        .add ( data.salesChannel )    //
        .add ( data.orderPriority )   //
        .add ( data.orderDate )       //
        .add ( data.orderId )         //
        .add ( data.shipDate )        //
        .add ( data.unitsold )        //
        .add ( data.unitPrice )       //
        .add ( data.unitPriceTotal )  //
        .add ( data.totalRev )        //
        .add ( data.totalCost )       //
        .add ( data.totalProfit )     //
        ;

    return data;
  }
};

class DataCsvStreamWriter final : public gd::csv::CsvStreamWriter< Data > {
 public:
  DataCsvStreamWriter ( std::ostream &stream ) : CsvStreamWriter< Data > ( stream ) {}

  void fromData ( gd::csv::CsvStringBuilder &builder, const Data &data ) override {
    builder                           //
        .add ( data.region )          //
        .addDelimiter ( )             //
        .add ( data.county )          //
        .addDelimiter ( )             //
        .add ( data.itemtype )        //
        .addDelimiter ( )             //
        .add ( data.salesChannel )    //
        .addDelimiter ( )             //
        .add ( data.orderPriority )   //
        .addDelimiter ( )             //
        .add ( data.orderDate )       //
        .addDelimiter ( )             //
        .add ( data.orderId )         //
        .addDelimiter ( )             //
        .add ( data.shipDate )        //
        .addDelimiter ( )             //
        .add ( data.unitsold )        //
        .addDelimiter ( )             //
        .add ( data.unitPrice )       //
        .addDelimiter ( )             //
        .add ( data.unitPriceTotal )  //
        .addDelimiter ( )             //
        .add ( data.totalRev )        //
        .addDelimiter ( )             //
        .add ( data.totalCost )       //
        .addDelimiter ( )             //
        .add ( data.totalProfit )     //
        ;
  }
};

static void BM_csv_read ( benchmark::State &state ) {
  //
  for ( auto _ : state ) {
    std::ifstream ifile ( "../testbenchmark/1500000 Sales Records.csv" );
    if ( ifile.good ( ) ) {
      // TODO: Read Benchmark
      DataCsvStreamReader csvStreamReader ( ifile );
      bool hasHeader = false;
      auto datalist = csvStreamReader.read ( hasHeader );
      ifile.close ( );

      // TODO: Write Benchmark
      std::ofstream ofile ( "../testbenchmark/outlargeData.csv" );
      DataCsvStreamWriter csvStreamWriter ( ofile );
      csvStreamWriter.write ( datalist );
      ofile.close ( );
    }
  }
}

BENCHMARK ( BM_csv_read );

BENCHMARK_MAIN ( );
