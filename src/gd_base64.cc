#include "gd/gd_base64.hh"
#include <cctype>
#include <cstdint>

namespace gd::base64 {

static const uint8_t base64_lookup[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const uint8_t base64_lookup_r[] = {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  62, 0,  0,  0,  63,  //
                                               52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0,  0,  0,  0,  0,  0,   //
                                               0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14,  //
                                               15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,  0,   //
                                               0,  26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  //
                                               41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,   //
                                               0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0};


constexpr bool is_base64_char(int ch) noexcept {
  return std::isalnum(ch) || ch == '+' || ch == '/';
}

Base64_t encode ( const char *inData, const size_t inDataLength ) noexcept {
  return encode ( ( const uint8_t * ) inData, inDataLength );
}
Base64_t encode ( const uint8_t *inData, const size_t inDataLength ) noexcept {
  std::size_t outLen = ( ( ( inDataLength / 3 ) + 1 ) * 4 ) + 1;

  uint8_t *out{nullptr};
  if ( !( out = new uint8_t[ outLen ] ) ) {
    return Base64_t ( std::unique_ptr< uint8_t[] > ( nullptr ), 0LL );
  }

  std::size_t i{0};
  const uint8_t *ptr{nullptr};
  const auto ptrLength{inData + inDataLength};
  for ( i = 0, ptr = inData; ptr < ptrLength; ptr += 3 ) {
    outLen = static_cast<size_t>(ptrLength - ptr);

    const auto inPtr = reinterpret_cast< const uint8_t * > ( &ptr[ 0 ] );
    out[ i++ ] = base64_lookup[ inPtr[ 0 ] >> 2 ];

    out[ i++ ] = base64_lookup[ ( ( inPtr[ 0 ] & 0x03 ) << 4 ) | ( outLen > 1 ? ( ( inPtr[ 1 ] & 0xf0 ) >> 4 ) : 0 ) ];

    out[ i++ ] =
        outLen > 1
            ? base64_lookup[ ( ( inPtr[ 1 ] & 0x0f ) << 2 ) | ( outLen > 2 ? ( ( inPtr[ 2 ] & 0xc0 ) >> 6 ) : 0 ) ]
            : '=';

    out[ i++ ] = outLen > 2 ? base64_lookup[ inPtr[ 2 ] & 0x3f ] : '=';
  }
  out[ i ] = 0;
  return Base64_t ( std::unique_ptr< uint8_t[] > ( out ), 0LL );
}

Base64_t decode ( const char *inData, const size_t inDataLength ) noexcept {
  return decode ( ( const uint8_t * ) inData, inDataLength );
}
Base64_t decode ( const uint8_t *inData, const size_t inDataLength ) noexcept {
  std::size_t outLen = ( ( ( inDataLength / 4 ) + 1 ) * 3 ) + 1;

  uint8_t *out{nullptr};
  if ( !( out = new uint8_t[ outLen ] ) ) {
    return Base64_t ( std::unique_ptr< uint8_t[] > ( nullptr ), 0LL );
  }

  std::size_t i{0};
  const uint8_t *ptr{nullptr};
  const auto ptrLength{inData + inDataLength};
  for ( i = 0, ptr = inData; ptr < ptrLength; ptr += 4 ) {
    outLen = static_cast<size_t>(ptrLength - ptr);

    const auto inPtr = reinterpret_cast< const uint8_t * > ( &ptr[ 0 ] );
    if ( inPtr[ 0 ] != '=' ) {
      const auto aa = base64_lookup_r[ inPtr[ 0 ] ] << 2;
      const auto bb = outLen > 0 && inPtr[ 1 ] != '=' ? ( base64_lookup_r[ inPtr[ 1 ] ] >> 4 ) : 0;
      out[ i++ ] = static_cast< uint8_t > ( aa | bb );
    }
    if ( outLen > 1 && inPtr[ 1 ] != '=' ) {
      const auto aa = ( base64_lookup_r[ inPtr[ 1 ] ] & 0x0f ) << 4;
      const auto bb = outLen > 1 && inPtr[ 2 ] != '=' ? ( base64_lookup_r[ inPtr[ 2 ] ] >> 2 ) : 0;
      out[ i++ ] = static_cast< uint8_t > ( aa | bb );
    }
    if ( outLen > 2 && inPtr[ 2 ] != '=' ) {
      const auto aa = ( base64_lookup_r[ inPtr[ 2 ] ] & 0x03 ) << 6;
      const auto bb = outLen > 2 && inPtr[ 3 ] != '=' ? ( base64_lookup_r[ inPtr[ 3 ] ] ) : 0;
      out[ i++ ] = static_cast< uint8_t > ( aa | bb );
    }
  }
  out[ i ] = 0;
  return Base64_t ( std::unique_ptr< uint8_t[] > ( out ), 0LL );
}

}  // namespace gd::base64
