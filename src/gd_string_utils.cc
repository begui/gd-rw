#include "gd/gd_string_utils.hh"

#include <algorithm>

namespace gd::string {

void replace_all ( std::string &str, const std::string &searchStr, const std::string &replaceStr ) noexcept {
  auto pos = str.find ( searchStr, 0 );
  while ( pos != std::string::npos ) {
    str.erase ( pos, searchStr.length ( ) );
    str.insert ( pos, replaceStr );
    pos = str.find ( searchStr, pos );
  }
}

std::string_view trim ( std::string_view strView ) noexcept {
  constexpr auto toTrim = " \f\t\v\r\n";
  strView.remove_prefix ( std::min ( strView.find_first_not_of ( toTrim ), strView.size ( ) ) );
  strView.remove_suffix ( std::min ( strView.size ( ) - strView.find_last_not_of ( toTrim ) - 1, strView.size ( ) ) );
  return strView;
}

}  // namespace gd::string
