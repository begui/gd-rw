#include "gd/gd_csv.hh"

namespace gd::csv {

namespace internal {
constexpr auto QUOTE{'"'};
constexpr auto isQuote = [] ( char c ) { return c == QUOTE; };
constexpr auto isCRLF = [] ( char c ) { return c == '\r' || c == '\n'; };
}  // namespace internal

std::vector< std::string_view > parse ( std::vector< std::string_view > &cells,  //
                                        const std::string &str,                  //
                                        const char delimiter ) {
  enum class State { Null, InCell, InCellWithQuote };
  State state = State::Null;
  for ( std::size_t i{0}; i < str.size ( ); ++i ) {
    const auto isDelimter = str[ i ] == delimiter;
    const auto isDoubleQuote = internal::isQuote ( str[ i ] ) && state == State::Null;
    const auto newState = ( isDelimter ? State::Null : isDoubleQuote ? State::InCellWithQuote : State::InCell );
    if ( newState != state ) {
      state = newState;
      switch ( state ) {
        case State::InCellWithQuote: {
          std::size_t ii = i;
          do {
            auto pos = str.find_first_of ( internal::QUOTE, ii + 1 );
            if ( pos == std::string::npos ) {
              throw std::runtime_error ( "Failed to parse string: {" + str + "}" );
            }
            if ( str[ pos + 1 ] == internal::QUOTE ) {
              // Quote within
              ii = pos + 1;
            } else if ( internal::isCRLF ( str[ pos ] ) ) {
              ii = pos;
            } else if ( str[ pos + 1 ] == delimiter || str[ pos + 1 ] == '\0' ) {
              cells.emplace_back ( std::string_view ( str ).substr ( i + 1, pos - 1 - i ) );
              i = pos;
              state = State::Null;
            } else {
              throw std::runtime_error ( "Failed to parse string: [" + str + "]" );
            }
          } while ( state == State::InCellWithQuote );
        } break;
        case State::InCell: {
          auto pos = str.find_first_of ( delimiter, i + 1 );
          if ( pos != std::string::npos ) {
            cells.emplace_back ( std::string_view ( str ).substr ( i, pos - i ) );
            i = pos;
            state = State::Null;
          } else {
            // this must be the end of string
            cells.emplace_back ( std::string_view ( str ).substr ( i, str.size ( ) - i ) );
          }
        } break;
        case State::Null:
        default:
          break;
      }
      if ( str[ i ] == delimiter && i == str.size ( ) - 1 ) {
        // this must be the end of string
        cells.emplace_back ( std::string_view ( str ).substr ( i, 0 ) );
      }
    } else {
      if ( isDelimter && ( i == 0 || str[ i - 1 ] == delimiter || i == str.size ( ) ) ) {
        cells.emplace_back ( std::string_view ( str ).substr ( i, 0 ) );
      }
    }
  }  // end of for

  return cells;
}

std::vector< std::string_view > parse ( const std::string &str, const size_t elements, const char delimiter ) {
  std::vector< std::string_view > cells;
  cells.reserve ( elements );
  return parse ( cells, str, delimiter );
}

}  // namespace gd::csv
